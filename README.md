# Between Green Hills and Green Bills: Unveiling the Green Shades of Sustainability and Burden Shifting through Multi-Objective Optimization in Swiss Energy System Planning

## Abstract
The Paris agreement is the first-ever universally accepted and legally binding agreement on global climate change. It is a bridge between todays and climate-neutrality policies and strategies before the end of the century. Critical to this endeavor is energy system modeling, which, while adept at devising cost-effective carbon-neutral strategies, often overlooks the broader environmental and social implications. This study introduces an innovative methodology that integrates life-cycle impact assessment indicators into energy system modeling, enabling a comprehensive assessment of both economic and environmental outcomes.
Focusing on Switzerland's energy system as a case study, our model reveals that optimizing key environomic indicators can lead to significant economic advantages, with system costs potentially decreasing by 15% to 47% by minimizing potential impacts from operating fossil technologies to the indirect impact related to the construction of the renewable infrastructure. However, a system optimized solely for economic efficiency, despite achieving 63% reduction in carbon footprint compared to 2020, our results show a potential risk of burden shift to other environmental issues.
The adoption of multi-objective optimization in our approach nuances the exploration of the complex interplay between environomic objectives and technological choices. Our results illuminate pathways towards more holistically optimized energy systems, effectively addressing trade-offs across environmental problems and enhancing societal acceptance of the solutions to this century's defining challenge.

## Introduction
This repository contains the model and result data for the corresponding paper.


## Structure

        .
    ├── 01_Model                                            # EnergyScope model (AMPL)
    ├── 02_Data                                             # Results data for the corresponding subsections
    │   ├── 3.2_Life_Cycle_Assessment
    │   ├── 4.1_Single-Objecive_Optimization
    │   └── 4.2_Multi-Objecive_Optimization  
    ├── LICENSE                     
    └── README.md


## Authors and acknowledgment
- [**Jonas Schnidrig**](mailto:jonas.schnidrig@hevs.ch): Corresponding author
- **Matthieu Souttre**: Data management, Methodology redaction, Validation and Review
- **Arthur Chuat**: Data management, Results validation, Validation and Review
- **François Maréchal**: Funding and Supervision
- **Manuele Margni**: Study conceptualization, Validation & Supervision


## License
MIT Licence

## Project status
Submitted for reviewing
