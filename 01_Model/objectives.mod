var TotalLCIA_CCEQL;
 subject to LCIA_CCEQL_cal:
   TotalLCIA_CCEQL = TotalLCIA['CCEQL'] + TotalCost*1e-6;

var TotalLCIA_CCEQS;
 subject to LCIA_CCEQS_cal:
   TotalLCIA_CCEQS = TotalLCIA['CCEQS'] + TotalCost*1e-6;

var TotalLCIA_FWA;
 subject to LCIA_FWA_cal:
   TotalLCIA_FWA = TotalLCIA['FWA'] + TotalCost*1e-6;

var TotalLCIA_FWEXL;
 subject to LCIA_FWEXL_cal:
   TotalLCIA_FWEXL = TotalLCIA['FWEXL'] + TotalCost*1e-6;

var TotalLCIA_FWEXS;
 subject to LCIA_FWEXS_cal:
   TotalLCIA_FWEXS = TotalLCIA['FWEXS'] + TotalCost*1e-6;

var TotalLCIA_FWEU;
 subject to LCIA_FWEU_cal:
   TotalLCIA_FWEU = TotalLCIA['FWEU'] + TotalCost*1e-6;

var TotalLCIA_IREQ;
 subject to LCIA_IREQ_cal:
   TotalLCIA_IREQ = TotalLCIA['IREQ'] + TotalCost*1e-6;

var TotalLCIA_LOBDV;
 subject to LCIA_LOBDV_cal:
   TotalLCIA_LOBDV = TotalLCIA['LOBDV'] + TotalCost*1e-6;

var TotalLCIA_LTBDV;
 subject to LCIA_LTBDV_cal:
   TotalLCIA_LTBDV = TotalLCIA['LTBDV'] + TotalCost*1e-6;

var TotalLCIA_MAL;
 subject to LCIA_MAL_cal:
   TotalLCIA_MAL = TotalLCIA['MAL'] + TotalCost*1e-6;

var TotalLCIA_MAS;
 subject to LCIA_MAS_cal:
   TotalLCIA_MAS = TotalLCIA['MAS'] + TotalCost*1e-6;

var TotalLCIA_MEU;
 subject to LCIA_MEU_cal:
   TotalLCIA_MEU = TotalLCIA['MEU'] + TotalCost*1e-6;

var TotalLCIA_TRA;
 subject to LCIA_TRA_cal:
   TotalLCIA_TRA = TotalLCIA['TRA'] + TotalCost*1e-6;

var TotalLCIA_TPW;
 subject to LCIA_TPW_cal:
   TotalLCIA_TPW = TotalLCIA['TPW'] + TotalCost*1e-6;

var TotalLCIA_TTEQ;
 subject to LCIA_TTEQ_cal:
   TotalLCIA_TTEQ = TotalLCIA['TTEQ'] + TotalCost*1e-6;

var TotalLCIA_WAVFWES;
 subject to LCIA_WAVFWES_cal:
   TotalLCIA_WAVFWES = TotalLCIA['WAVFWES'] + TotalCost*1e-6;

var TotalLCIA_WAVTES;
 subject to LCIA_WAVTES_cal:
   TotalLCIA_WAVTES = TotalLCIA['WAVTES'] + TotalCost*1e-6;

var TotalLCIA_REQD;
 subject to LCIA_REQD_cal:
   TotalLCIA_REQD = TotalLCIA['REQD'] + TotalCost*1e-6;

var TotalLCIA_CCHHL;
 subject to LCIA_CCHHL_cal:
   TotalLCIA_CCHHL = TotalLCIA['CCHHL'] + TotalCost*1e-6;

var TotalLCIA_CCHHS;
 subject to LCIA_CCHHS_cal:
   TotalLCIA_CCHHS = TotalLCIA['CCHHS'] + TotalCost*1e-6;

var TotalLCIA_HTXCL;
 subject to LCIA_HTXCL_cal:
   TotalLCIA_HTXCL = TotalLCIA['HTXCL'] + TotalCost*1e-6;

var TotalLCIA_HTXCS;
 subject to LCIA_HTXCS_cal:
   TotalLCIA_HTXCS = TotalLCIA['HTXCS'] + TotalCost*1e-6;

var TotalLCIA_HTXNCL;
 subject to LCIA_HTXNCL_cal:
   TotalLCIA_HTXNCL = TotalLCIA['HTXNCL'] + TotalCost*1e-6;

var TotalLCIA_HTXNCS;
 subject to LCIA_HTXNCS_cal:
   TotalLCIA_HTXNCS = TotalLCIA['HTXNCS'] + TotalCost*1e-6;

var TotalLCIA_IRHH;
 subject to LCIA_IRHH_cal:
   TotalLCIA_IRHH = TotalLCIA['IRHH'] + TotalCost*1e-6;

var TotalLCIA_OLD;
 subject to LCIA_OLD_cal:
   TotalLCIA_OLD = TotalLCIA['OLD'] + TotalCost*1e-6;

var TotalLCIA_PMF;
 subject to LCIA_PMF_cal:
   TotalLCIA_PMF = TotalLCIA['PMF'] + TotalCost*1e-6;

var TotalLCIA_PCOX;
 subject to LCIA_PCOX_cal:
   TotalLCIA_PCOX = TotalLCIA['PCOX'] + TotalCost*1e-6;

var TotalLCIA_TTHH;
 subject to LCIA_TTHH_cal:
   TotalLCIA_TTHH = TotalLCIA['TTHH'] + TotalCost*1e-6;

var TotalLCIA_WAVHH;
 subject to LCIA_WAVHH_cal:
   TotalLCIA_WAVHH = TotalLCIA['WAVHH'] + TotalCost*1e-6;

var TotalLCIA_RHHD;
 subject to LCIA_RHHD_cal:
   TotalLCIA_RHHD = TotalLCIA['RHHD'] + TotalCost*1e-6;

var TotalLCIA_CF;
 subject to LCIA_CF_cal:
   TotalLCIA_CF = TotalLCIA['CF'] + TotalCost*1e-6;

var TotalLCIA_FNEU;
 subject to LCIA_FNEU_cal:
   TotalLCIA_FNEU = TotalLCIA['FNEU'] + TotalCost*1e-6;

var TotalLCIA_WSF;
 subject to LCIA_WSF_cal:
   TotalLCIA_WSF = TotalLCIA['WSF'] + TotalCost*1e-6;

